# known issues

This is a compilation of known issues with this system.

## open

### 1. backend - /v0/accounts/N - filtered_totals doesn't exclude campaigns outside of limits

_Repro Steps:_  
Set the limit query param (i.e. /v0/accounts/0?limit=1)  
_Expected:_  
filtered_totals will match the single campaign returned
_Actual:_
filtered totals reflect the entire accounts totals

### 2. backend - /v0/accounts/N - returns a payload with an error message when filter is too strict

_Repro Steps:_  
Set the filter query param to something crazy (i.e. /v0/accounts/0?filter=lkjlkjlkjlkjl)  
_Expected:_  
successful result with empty campaigns array
_Actual:_
error message in payload

### 3. frontend - hardcoded error message and fake data insertion to workaround issue 2

The frontend does a hardcoded check for the specific error message provided in inssue 2, and uses some fake data in these scenarios. When issue 2 is fixed, this should be fixed as well.

### 4. frontend - table header medium small screens

the table header does not render correctly on screens less than 1020px wide

### 5. frontend - table values medium small screens

the campaigns table does not handle large monetary values well on screens less than 1020px wide

### 6. frontend - top level layout needs refactor

the header and the table header should not be part of different sections of they layout