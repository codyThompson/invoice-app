# technical specs.

This document is meant to be a rough attempt at both outlining the goals (requirements) of this project, as well as providing some design and implementation guidance.

## Goals

The web app should provide users with the ability to view and edit an invoice with a large (more than will fit on a screen) number of line items. The following criteria can be used to determine the success of this project: 

1. The web app is implemented in a few hours.
2. The web app is easily installed, built, and run
3. The web app has a dazzling UI
4. The web app implements the following use cases:
    1. **bucket 1**
        1. The user should be able to see sub-totals grouped by campaign (line-items grouped by their parent campaign)
        2. The user should be able to see the invoice grand-total (sum of each line-item's billable amount).  
        3. The user should be able to sort the data. 
    2. **bucket 2**
        1. The user should be able to filter the data (ie. by campaign name, etc., should affect the grand-total).
5. The web app should have a test suite that attempts to verify the behavior listed in the previous bullet.

## design/plan

The web app's implementation will be separated into three sub projects:

- setup
- backend
- frontend

The setup script will create the database and any other files/services the rest api backend will need.

The backend API will retrieve data from the database, and deliver that data in a format that is useful for the applications frontend.

The frontend will display the information from the API and provide a user interface for manipulating how that information is displayed.

### backend

#### Database Engine

We'll use sqllite db for ease of setup and use on a normal mac/pc. NoSQL option was considered as the sample data seemed like it might fit a nosql db better, but some cursory googling yielded no recommended lightweight NoSQL options.

#### Language/Environment

- NodeJS version 8
    - Version 8 has support for a lot of useful es6 features out of the box, and is the current long-term-support version of node.
- express
- sqllite driver/client
    - https://github.com/rneilson/node-sqlite-pool 
        - pools connections by default
        - returns promises

#### Schema/Tables

- account
    - id
        - PK
  - name
      - string
      - required

- campaign
    - id
        - PK
    - account_id
        - FK
    - name
        - string
        - required
    
- line_item
    - id
        - PK
    - campaign_id
        - FK
    - name
        - string
        - required
    - booked
        - double
        - required
    - actual
        - double
        - required
  
- line_item_totals
    - id
        - PK
    - line_item_id
        - FK
    - amount
        - double
        - required
    - note
        - string
        - default null

- account_totals (view)
    - account_id
    - booked
    - actual
    - adjustments
    - billable

- campaign_totals (view)
    - account_id
    - campaign_id
    - booked
    - actual
    - adjustments
    - billable

- adjustments (view)
    - line_item_id
    - adjustments

#### Endpoints

- /v0/accounts/:account_id 
    - options
        - sort_field: \<name|booked|actual|adjustments|billable\>
            - default: billable
        - sort_dir: \<asc|desc\>
            - default: asc
        - filter: string
        - offset: positive integer
        - limit: positive integer
    - computed values
        - booked = sum(campaigns.totals.booked)
        - actual = sum(campaigns.totals.actual)
        - adjustments = sum(campaigns.totals.adjustments)
        - billable = sum(campaigns.totals.billable)
        - campaign.totals.booked = sum(campaign.line_items.totals.booked)
        - campaign.totals.actual = sum(campaign.line_items.totals.actual)
        - campaign.totals.adjustments = sum(campaign.line_items.totals.adjustments)
        - campaign.totals.billable = sum(campaign.line_items.totals.billable)
    - allowed verbs
        - GET
    - json schema:  
```
{
  id: int,
  name: string,
  totals: {
    booked: double,
    actual: double,
    adjustments: double,
    billable: double
  },
  filtered_totals: {
    booked: double,
    actual: double,
    adjustments: double,
    billable: double
  },
  campaigns: [
    {
      id: int,
      name: string,
      totals: {
        booked: double,
        actual: double,
        adjustments: double,
        billable: double
      }
    }
  ]
}
```

#### Backend Testing / Acceptance Criteria

- _goal 4.1.1_
    - The backend should populate/create correctly formatted json
        - it should result with json that passes a schema based json validator
    - The rest api should return the expected values
- _goal 4.1.2_
    - The backend should calculate values correctly
        - it should calculate an account's booked total correctly
        - it should calculate an account's actual total correctly
        - it should calculate an account's adjustments total correctly
        - it should calculate an account's billable total correctly
        - it should calculate a campaign's booked total correctly
        - it should calculate a campaign's actual total correctly
        - it should calculate a campaign's adjustments total correctly
        - it should calculate a campaign's billable total correctly
- _goal 4.1.3_
    - The backend should populate/create json with correctly sorted values
        - it should respect the sort_field parameter
        - it should respect the sort_order parameter
- _goal 4.2.1_
    - The backend should populate/create json with correctly filtered values 
        - it should respect the filter parameter

### frontend

We need to make a frontend application that will make use of our backend API to provide a UI for the use cases described in _goal 4_. We will be using webpack/React to satisfy _goal 1,2_, and Ant Design library to try and satisfy _goal 3_ with minimal effort (_goal 1_).

#### datastore

If this were a production application, we would absolutely want to use a datastore of some description, and probably not want to roll our own. Due to the scope of this project and _goal 1_, we're going to skip this step and maintain application state within component logic.

#### frontend tests

The data store should have some basic unit tests.

### setup

To statisfy _goal 2_ we'll need installation/usage doc(s), and a way to setup the database "from scratch".

A script will need to be created that will
- create the local sqllite instance 
- create the db schema
- create any required data
- load in initial data from a file

This script will be written in javascript to avoid the need to have the user install a different scripting language interpreter.