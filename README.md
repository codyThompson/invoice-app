# invoice-app

## Brief statement on decisions made

### Use-case choices

My use-case selections seemed to focus on aggregating values. It had been a while since I stretched out my sql muscles, so I enjoyed re-learning how to aggregate across multiple 1 to many relationshipts.

You can also do a lot of cool stuff UI wise with nested data (i.e. nested tables with fun slide out animations). Unfortunately, it took me longer that I had hoped to get the account and campaign table "wired up" correctly on the backend, and I didn't have time to add nested tables for line items. Best-laid plans aside, I still had a lot of fun setting up this UI. 

### lib/framework decisions

- Node/JavaScript  
I chose node and javascript for two reasons
    1. I'm very familiar with it and wanted to put my best foot forward.
    2. I've been trying to adopt more functional programming practices lately, and modern javascript has a lot of cool featrues in this department.
- sqlite  
    - I chose sqlite for it's ease of setup and low configuration. While I really enjoyed not having to install a whole DB engine, or having to get a remote/cloud service set up, I did have more issues than I'd care to admit picking a node sqlite driver/wrapper (and using it correctly). I ended up going with a lib called sqlite-pool because it supported connection pools, transactions, and JS Promises. 
- express
- React
    - I've been using proprietary JS frameworks for 4 years now. Because of this, I have been set a personal goal of learning React on an idiomatic level. This was a great opportunity to continue a little farther down that path.
- AntD
    - It's like bootstrap for React. There are probably alternatives, but this just happens to be the one I know about.
- Jest
    - To be honest, I chose this because it comes packaged with the react boiler plate project and I think it has a 'pretty' CLI.

## Installation

1. Clone the repo  
    1. Open a terminal in your repositories folder
    2. run  
    `git clone git@bitbucket.org:codyThompson/invoice-app.git`
2. Please ensure that you have NodeJS version 8 or later installed.
    - If you already have an earlier version of node installed, [nvm](https://github.com/creationix/nvm) (Node Version Manager) is a useful tool for managing multiple node versions.  
    NOTE: NVM isn't supported on windows, but there are some unofficial clones, and you might be able to use it on the WLS.
    - If you don't have NodeJS installed, please follow the instructions on the [node website](https://nodejs.org/en/download/)
3. Install the setup script
    1. Open a terminal in `invoice-app/setup`
    2. run `npm install` or `npm i`
4. Run the setup script
    1. With your terminal still open in `invoice-app/setup`, run  
    `node setup.js`  
    If no error messages print out, you should have a sqlite database created and populated on your computer.
5. Install the backend
   1. `cd` into `invoice-app/backend`
   2. run `npm i`  
6. Install the frontend
   1. `cd` into `invoice-app/frontend`
   2. run `npm i`

## Usage

You must start both the backend and frontend servers for the application to run correctly.

1. open a terminal in `invoice-app/backend`
    1. run `npm start`
    2. leave the terminal open and running
2. open a different terminal in `invoice-app/frontend`
    1. run `npm start`
    2. when prompted about a port number, simply type `y` and press enter
3. a browser tab will _probably_ eventually open up with the web application displayed. If not, open up localhost:3001 in you browser.

## Testing

running `npm test` in the `invoice-app/backend` and `invoice-app/frontend` folders should run a test suite (that should pass).
NOTE: Timeout issues (I'm assuming :/ ) will occasionally cause the backend test suite to fail. If you simply run `npm test` again, the tests should all pass.

## Docs

There are some (very) rough specs in docs folder, along with a list of known issues with the app.
