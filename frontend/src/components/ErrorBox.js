import React from 'react';
import Alert from 'antd/lib/alert';

import 'antd/lib/alert/style/css';

export default ({message="An error occurred"}) => 
  <div className="ErrorBox" style={{padding: '2%'}} >
    <Alert
      message="Error"
      description={message}
      type="error"
      showIcon />
  </div>