import React, {Component} from 'react';

export default (LoadingComponent, ErrorComponent, NormalComponent, dataStore, dataGroupName, dataPropName) =>
  class extends Component {
    constructor(props) {
      super(props);

      this.state = {
        dataGroup: dataStore.getData(dataGroupName)
      };

    }

    componentDidMount = () => {
      dataStore.addChangeListener(dataGroupName, this.onDataChange);
    }

    componentWillUnmount = () => {
      dataStore.removeChangeListener(dataGroupName, this.onDataChange);
    }

    componentDidCatch = () => {
      dataStore.errorHappened(dataGroupName);
    }

    onDataChange = (eventName, dataGroup) => {
      this.setState({dataGroup})
    }

    render = () => {
      const dataGroup = this.state.dataGroup;
      const {isLoading, inError, content} = dataGroup;
      const props = {...this.props, [dataPropName]: content};
      if (isLoading) {
        return <LoadingComponent dataGroup={dataGroup} />
      } else if (inError) {
        return <ErrorComponent dataGroup={dataGroup} />
      } else {
        return <NormalComponent {...props} />
      }
    }
  }