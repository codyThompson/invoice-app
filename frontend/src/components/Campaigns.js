import React, {Component} from 'react';
import {debounce} from 'lodash';
import Table from 'antd/lib/table';
import Input from 'antd/lib/input';
import Icon from 'antd/lib/icon';
import { Row, Col } from 'antd';

import TotalsHeader from './TotalsHeader';

import 'antd/lib/table/style/css';
import 'antd/lib/input/style/css';
import 'antd/lib/icon/style/css';
import 'antd/lib/row/style/css';
import 'antd/lib/col/style/css';
import '../css/Campaigns.css';

const FILTER_DEBOUNCE_MS = 100;

const columnMap = [
  {
    title: 'id',
    dataIndex: 'id',
    key: 'id',
    sorter: (a, b) => a.id - b.id
  },
  {
    title: 'name',
    dataIndex: 'name',
    key: 'name',
    sorter: (a, b) => a.name.localeCompare(b.name)
  },
  {
    title: 'booked',
    dataIndex: 'bookedFormatted',
    key: 'bookedFormatted',
    sorter: (a, b) => a.booked - b.booked
  },
  {
    title: 'actual',
    dataIndex: 'actualFormatted',
    key: 'actualFormatted',
    sorter: (a, b) => a.actual - b.actual
  },
  {
    title: 'adjustments',
    dataIndex: 'adjustmentsFormatted',
    key: 'adjustmentsFormatted',
    sorter: (a, b) => a.adjustments - b.adjustments
  },
  {
    title: 'billable',
    dataIndex: 'billableFormatted',
    key: 'billableFormatted',
    sorter: (a, b) => a.billable - b.billable
  }
];

class Campaigns extends Component {
  formatCurrency = (value) => {
    if (!this.props.currencyFormatter) {
      return value;
    }
    return this.props.currencyFormatter.format(value);
  }

  formatRowData = (campaignRows=[]) => {
    let flattened = campaignRows
      .map((row) => {
        return {
          id: row.id,
          key: row.id,
          name: row.name,
          ...row.totals,
          bookedFormatted: this.formatCurrency(row.totals.booked),
          actualFormatted: this.formatCurrency(row.totals.actual),
          adjustmentsFormatted: this.formatCurrency(row.totals.adjustments),
          billableFormatted: this.formatCurrency(row.totals.billable)
        };
      });
    return flattened;
  }

  createDebounced = (...args) => {
    const debounced = debounce(...args).bind(this);
    return (e) => {
      e.persist();
      return debounced(e);
    }
  }

  onFilterChange = (e) => {
    this.props.onFilterChange && this.props.onFilterChange(e.target.value);
  }

  render = () => {
    const dataSource = this.formatRowData(this.props.extData.campaigns);
    return (
      <div className="Campaigns">
        <TotalsHeader extData={this.props.extData} totalsKey="filtered_totals">
          <Row>
            <Col sm={8}>
              Filtered Totals
            </Col>
            <Col sm={15}>
              <Input
                prefix={<Icon type="filter" style={{ color: 'rgba(0,0,0,.25)' }} />}
                size="small"
                placeholder="filter"
                onChange={this.createDebounced(this.onFilterChange, FILTER_DEBOUNCE_MS)}/>
            </Col>
          </Row>
        </TotalsHeader>
        <Table dataSource={dataSource} columns={columnMap} size="small" />
      </div>
    )
  }
};

// TODO - find a way to compose this into components
// this was copy pasted from TotalsHeader
Campaigns.defaultProps = {
  currencyFormatter: new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD'
  }),
  bookedLabel: 'booked',
  actualLabel: 'actual',
  adjustmentsLabel: 'adjustments',
  billableLabel: 'billable'
};

export default Campaigns;