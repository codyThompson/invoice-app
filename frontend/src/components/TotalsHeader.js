import React, {Component} from 'react';
import {Row, Col} from 'antd';

import 'antd/lib/row/style/css';
import 'antd/lib/col/style/css';

import '../css/TotalsHeader.css';

class TotalsHeader extends Component {

  formatCurrency = (value) => {
    if (!this.props.currencyFormatter) {
      return value;
    }
    return this.props.currencyFormatter.format(value);
  }

  get totals () {
    return this.props.extData[this.props.totalsKey];
  }

  get booked () {
    return this.formatCurrency(this.totals.booked);
  }
  get actual () {
    return this.formatCurrency(this.totals.actual);
  }
  get adjustments () {
    return this.formatCurrency(this.totals.adjustments);
  }
  get billable () {
    return this.formatCurrency(this.totals.billable);
  }

  render () {
    const name = this.props.extData.name;
    const {booked, actual, adjustments, billable} = this;
    return (
      <div className="TotalsHeader">
        <Row>
          { this.props.children ?
            <Col className="headerDescription" md={8} sm={24} xs={24}>
              {this.props.children}
            </Col>
          :
            <Col md={8} sm={24} xs={24}>
              <h2>{name}</h2>
            </Col>
          }
          <Col md={4} sm={6} xs={24}>
            <div>{booked}</div>
            <div className="label">{this.props.bookedLabel}</div>
          </Col>
          <Col md={4} sm={6} xs={24}>
            <div>{actual}</div>
            <div className="label">{this.props.actualLabel}</div>
          </Col>
          <Col md={4} sm={6} xs={24}>
            <div>{adjustments}</div>
            <div className="label">{this.props.adjustmentsLabel}</div>
          </Col>
          <Col md={4} sm={6} xs={24}>
            <div>{billable}</div>
            <div className="label">{this.props.billableLabel}</div>
          </Col>
        </Row>
      </div>
    )
  }
};

TotalsHeader.defaultProps = {
  totalsKey: 'totals',
  currencyFormatter: new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD'
  }),
  bookedLabel: 'booked',
  actualLabel: 'actual',
  adjustmentsLabel: 'adjustments',
  billableLabel: 'billable'
}

export default TotalsHeader;