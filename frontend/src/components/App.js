import React, { Component } from 'react';
import {Layout} from 'antd';
import 'antd/lib/layout/style';

import apiWrapper from '../apiWrapper';
import appDataStore, { constants as dsConstants } from '../appDataStore';
import TotalsHeader from './TotalsHeader';
import Campaigns from './Campaigns';
import Loading from './LoadingSpinner';
import ErrorBox from './ErrorBox';
import withLoading from './withLoading';
import '../css/App.css';


const {Header, Content, Footer} = Layout;
const TotalsHeaderWithLoading = withLoading(Loading, ErrorBox, TotalsHeader, appDataStore.dataStore, dsConstants.ACCOUNT, 'extData');
const CampaignsWithLoading = withLoading(Loading, ErrorBox, Campaigns, appDataStore.dataStore, dsConstants.ACCOUNT, 'extData');

class App extends Component {
  constructor(props) {
    super(props);
    this.appDataStore = appDataStore;
  }
  
  fetchAccount = (filter='') => {
    // hardcoded account id because I
    // didn't build a user system ;)
    apiWrapper.getAccount(0, {filter})
      // .then(({name, totals}) => {
      //   return {name, ...totals};
      // })
      .then(appDataStore.accountSetContent)
      .catch(appDataStore.accountErrorHappened)
  }

  onFilterChange = (newFilter) => {
    // appDataStore.accountLoadingStarted();
    this.fetchAccount(newFilter);
  }

  componentWillMount = () => {
    if (appDataStore.accountGetData().isLoading) {
      this.fetchAccount();
    }
  }

  render() {
    return (
      <div className="App">
        <Layout>
          <Header>
            <TotalsHeaderWithLoading />
          </Header>
          <Content>
            <CampaignsWithLoading onFilterChange={this.onFilterChange} />
          </Content>
          <Footer />
        </Layout>
      </div>
    );
  }
}

export default App;
