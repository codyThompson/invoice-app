import React from 'react';
import Spin from 'antd/lib/spin';
import Alert from 'antd/lib/alert';

import 'antd/lib/spin/style/css';
import 'antd/lib/alert/style/css';

export default ({message="loading panel content"}) =>
  <div className="LoadingSpinner" style={{padding: '2%'}}>
    <Spin tip={message} >
      <Alert type="info" style={{minHeight: '100px'}} />
    </Spin>
  </div>