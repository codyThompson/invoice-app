const apiWrapper = {
  useAbsolute: true,
  protocol: 'http',
  base: 'localhost',
  port: 3000,
  accountPath: '/v0/accounts/',

  formatURI (path, entityId, queryParams={}) {
    let baseURI = '';
    if (apiWrapper.useAbsolute) {
      const {protocol, base, port} = apiWrapper;
      baseURI = `${protocol}://${base}:${port}`;
    }
    const queryStr = Object.keys(queryParams)
      .map(key => `${key}=${queryParams[key]}`)
      .join('&');
    return `${baseURI}${path}${entityId}?${queryStr}`;
  },

  getAccount (accountId, queryParams) {
    const uri = apiWrapper.formatURI(this.accountPath, accountId, queryParams);
    return fetch(uri)
      .then(result => result.json())
      .then(result => {
        if (result.success) {
          return Promise.resolve(result.result);
        } else {
          // TODO: this is super gross. The endpoint needs to behave differently in this scenario
          if (result.errors[0] === 'no account information available for the given account id and/or query') {
            result.result = {
              id: 0,
              name: '',
              totals: {
                booked: 0,
                actual: 0,
                adjustments: 0,
                billable: 0
              },
              filtered_totals: {
                booked: 0,
                actual: 0,
                adjustments: 0,
                billable: 0
              },
              campaigns: []
            };
            return Promise.resolve(result.result);
          }
          return Promise.reject(result.errors);
        }
      });
  }
}

export default apiWrapper;