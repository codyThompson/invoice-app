import DataStore from './SimpleDataStore';

/**
 * This file sets up the data store and adds
 * some convenience functions
 */

const constants = {
  ACCOUNT: 'account'
};

// I'm doing data validation in a reducer pattern,
// probably a bad idea, but it will work for now
const totalsValidator = (prevState, newState) => {
  const expectedProps = {
    booked: 'number',
    actual: 'number',
    adjustments: 'number',
    billable: 'number'
  };

  let validNewState = Object.keys(newState.totals)
    .every(key => key in expectedProps && typeof newState.totals[key] === expectedProps[key]);

  if (!validNewState) {
    console.error('malformed data received');
    throw new Error('malformed totals data');
  }

  return {...newState};
};

const dataStore = new DataStore()
  .addDataGroup(constants.ACCOUNT)
  .setReducer(constants.ACCOUNT, totalsValidator);

const dsMethodWith = (dataGroupName, method) =>
  method.bind(dataStore, dataGroupName);

const dsErrorHappenedWith = (dataGroupName) =>
  (e={}) => {
    const message = `[appDataStore] An error was reported for ${dataGroupName}\nMESSAGE:\n${e.message}\nSTACK:\n${e.stack}`;
    console.error(message);
    return dataStore.errorHappened(dataGroupName);
  };

const dsMethodWithAccount = dsMethodWith.bind(dataStore, constants.ACCOUNT);

const appDataStore = {
  dataStore,
  accountSetContent: dsMethodWithAccount(dataStore.setContent),
  accountLoadingStarted: dsMethodWithAccount(dataStore.loadingStarted),
  accountAddChangeListener: dsMethodWithAccount(dataStore.addChangeListener),
  accountRemoveChangeListener: dsMethodWithAccount(dataStore.removeChangeListener),
  accountGetData: dsMethodWithAccount(dataStore.getData),
  accountErrorHappened: dsErrorHappenedWith(constants.ACCOUNT)
};

// DEBUG CODE, REMOVE ME
window.dataStore = dataStore;
window.appDataStore = appDataStore;

export { constants };
export default appDataStore;