const DEFAULT_REDUCER = (prevState, newState) => {
  return {...newState}
};

/**
 * 
 */
class SimpleDataStore {
  data = { }

  addDataGroup = (groupName) => {
    this.data[groupName] = {
      name: groupName,
      isLoading: true,
      inError: false,
      changeListeners: [],
      reducer: DEFAULT_REDUCER,
      content: null
    };
    return this;
  }

  setReducer = (groupName, reducer) => {
    this.data[groupName].reducer = reducer;
    return this;
  }

  addChangeListener = (groupName, listener) => {
    this.data[groupName].changeListeners.push(listener);
    return this;
  }

  removeChangeListener = (groupName, listener) => {
    const dataGroup = this.getData(groupName);
    dataGroup.changeListeners = dataGroup.changeListeners
      .filter(inList => listener !== inList);
  };

  setContent = (groupName, content) => {
    const dataGroup = this.data[groupName];
    let prevState = dataGroup.content;
    try {
      dataGroup.content = dataGroup.reducer(dataGroup.content, content);
    } catch (e) {
      dataGroup.content = prevState;
      this.errorHappened(groupName);
      return;
    }
    dataGroup.isLoading = false;
    dataGroup.inError = false;
    this.fireChangeEvent(groupName, 'contentSet');
    return this;
  }

  loadingStarted = (groupName) => {
    const dataGroup = this.data[groupName];
    dataGroup.isLoading = true;
    dataGroup.inError = false;
    this.fireChangeEvent(groupName, 'loadingStarted');
    return this;
  }

  errorHappened = (groupName) => {
    const dataGroup = this.data[groupName];
    dataGroup.inError = true;
    dataGroup.isLoading = false;
    this.fireChangeEvent(groupName, 'errorHappened');
    return this;
  }

  fireChangeEvent = (groupName, eventName) => {
    requestAnimationFrame(() => {
      const dataGroup = this.getData(groupName);
      dataGroup.changeListeners
        .forEach(listener => {
          listener(eventName, dataGroup)
        });
    });
  }

  getData = (groupName) => {
    return this.data[groupName];
  }
}

export default SimpleDataStore;
export { DEFAULT_REDUCER };