import DataStore, { DEFAULT_REDUCER } from '../SimpleDataStore';

describe('SimpleDataStore', () => {
  it('should set the loading state', () => {
    return new Promise((resolve, reject) => {
      const ds = new DataStore()
        .addDataGroup('test1');
      expect(ds.getData('test1').isLoading)
        .toBe(true);
        ds
          .addChangeListener('test1', (eventName, dataGroup) => {
            if (eventName === 'loadingStarted') {
              try {
                expect(dataGroup.isLoading)
                  .toBe(true);
                expect(dataGroup.inError)
                  .toBe(false);
                resolve();
              } catch (e) {
                reject(e);
              }
            }
          })
          .setContent('test1', 'athing')
          .loadingStarted('test1');
    });
  });

  it('should set the content', () => {
    return new Promise((resolve, reject) => {
      new DataStore()
        .addDataGroup('test1')
        .setContent('test1', { hello: 'world' })
        .addChangeListener('test1', (eventName, data) => {
          try {
            expect(eventName)
              .toBe('contentSet');
            expect(data.isLoading)
              .toBe(false);
            expect(data.inError)
              .toBe(false);
            expect(data.content)
              .toEqual({ hello: 'world' });
            resolve();
          } catch (e) {
            reject(e);
          }
        });
    });
  });

  it('should set the in error state', () => {
    return new Promise((resolve, reject) => {
      new DataStore()
        .addDataGroup('test1')
        .addChangeListener('test1', (eventName, data) => {
          try {
            expect(eventName)
              .toBe('errorHappened');
            expect(data.isLoading)
              .toBe(false);
            expect(data.inError)
              .toBe(true);
            resolve();
          } catch (e) {
            reject(e);
          }
        })
        .errorHappened('test1');
    });
  });

  it('should add and remove change listeners', () => {
    const dsGroup = 'test4';
    return new Promise((resolve, reject) => {
      let ds = new DataStore();
      let fireCnt1 = 0;
      let fireCnt2 = 0;
      const chgListener1 = () => {
        if (++fireCnt1 > 1) {
          reject(new Error('called after removal'));
        }
      };
      const chgListener2 = () => {
        if (++fireCnt2 === 1) {
          try {
            ds.removeChangeListener(dsGroup, chgListener1);
            ds.fireChangeEvent(dsGroup, 'testEvent');
          } catch (e) {
            reject(e);
          }
        } else {
          resolve();
        }
      };
      ds
        .addDataGroup(dsGroup)
        .addChangeListener(dsGroup, chgListener1)
        .addChangeListener(dsGroup, chgListener2)
        .fireChangeEvent(dsGroup, 'testEvent');
    });
  });

  it('should treat a reducer fail as an errorHappened', () => {
    return new Promise((resolve, reject) => {
      const dsGroup = 'test5';
      let ds = new DataStore()
        .addDataGroup(dsGroup)
        .setReducer(dsGroup, () => {
          throw new Error('reducer thingy bad happened');
        })
        .addChangeListener(dsGroup, (eventName, dataGroup) => {
          try {
            expect (eventName)
              .toBe('errorHappened');
            expect (dataGroup.isLoading)
              .toBe(false);
            expect (dataGroup.inError)
              .toBe(true);
            resolve();
          } catch (e) {
            reject(e);
          }
        })
        .setContent(dsGroup, {});
    });
  });
});