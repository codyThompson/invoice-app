const path = require('path');

module.exports = {
  // path to the sqlite db file
  dbFilePath: path.resolve(__dirname, '../invoice-app.sqlite'),

  // path to the json data file
  dataFilePath: path.resolve(__dirname, './placements_teaser_data.json'),
  // dataFilePath: path.resolve(__dirname, './truncated_test_data.json'),
  // dataFilePath: path.resolve(__dirname, './smaller_test_data.json'),

  // path the sqlite migration files containing the schema and initial data
  migrationsPath: path.resolve(__dirname, './migrations'),

  // PK of the account id to associate all of the data with
  accountId: 0,

  // max amount of rows to insert in one statement
  maxRows: 100,

  // if true will delete the existing db
  deleteExisting: true
};