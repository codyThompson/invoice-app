-- UP

CREATE TABLE account (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL
);
CREATE TABLE campaign (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  account_id INTEGER NOT NULL,
  name TEXT NOT NULL,
    CONSTRAINT account_fk_id FOREIGN KEY (account_id)
      REFERENCES account (id)
);
CREATE TABLE line_item (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  campaign_id INTEGER NOT NULL,
  name TEXT NOT NULL,
  booked REAL NOT NULL,
  actual REAL NOT NULL,
    CONSTRAINT campaign_fk_id FOREIGN KEY (campaign_id)
      REFERENCES campaign (id)
);
CREATE TABLE adjustment (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  line_item_id INTEGER NOT NULL,
  amount REAL NOT NULL,
  note text,
    CONSTRAINT line_item_fk_id FOREIGN KEY (line_item_id)
      REFERENCES line_item (id)
);
CREATE VIEW line_item_totals AS
SELECT
line_item.id AS line_item_id,
SUM (adjustment.amount) AS adjustments
FROM adjustment
JOIN line_item ON line_item.id = adjustment.line_item_id
GROUP BY line_item_id;

CREATE VIEW campaign_totals AS
SELECT
  account.id as account_id,
  campaign.id as campaign_id,
  sum(line_item.booked) as booked,
  sum(line_item.actual) as actual,
  sum(line_item_totals.adjustments) as adjustments,
  sum(line_item.actual + line_item_totals.adjustments) as billable
FROM line_item
join line_item_totals on line_item_totals.line_item_id = line_item.id
join campaign on campaign.id = line_item.campaign_id
join account on account.id = campaign.account_id
group by account.id, campaign.id;

CREATE VIEW account_totals AS
SELECT
  account_id as account_id,
  sum(booked) as booked,
  sum(actual) as actual,
  sum(adjustments) as adjustments,
  sum(billable) as billable
FROM campaign_totals
GROUP BY account_id;

-- add fake account
INSERT INTO account VALUES (0, 'fake account')

-- Down
DROP TABLE account;
DROP TABLE campaign;
DROP TABLE line_item;
DROP TABLE adjustment;
DROP VIEW line_item_totals;
DROP VIEW campaign_totals;
DROP VIEW account_totals;