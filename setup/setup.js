const fs = require('fs-extra');
const path = require('path');
const Sqlite = require('sqlite-pool');
const uniqBy = require('lodash/uniqBy');
const chunk = require('lodash/chunk');
const flatten = require('lodash/flatten');
const defaults = require('lodash/defaults');

/**
 * Parses the given json file into an array.
 * @param {string} fileName path to the json data file
 * @returns {Promise} returns a promise
 */
const readDataFile = (fileName) => {
  return new Promise((resolve, reject) => {
    fs.readFile(fileName, 'utf-8')
      .then((dataStr) => {
        try {
          parsedData = JSON.parse(dataStr);
          resolve(parsedData);
        } catch (e) {
          reject(new Error(`unable to parse file "${fileName}" into JSON\nmessage:${e.message}`));
        }
      })
      .catch(e => reject(new Error(`unable to read file ${fileName}\nmessage:${e.message}`)));
  });
};

/**
 * Maps data from the json array into an array
 * of values representing the rows of the campaigns table
 * @param {integer} accountId 
 * @param {Array} dataArray 
 */
const getCampaignRows = (accountId, dataArray) => {
  return uniqBy(dataArray, 'campaign_id')
    // rows in the campaign table are
    // id PK, account_id FK, name
    .map(row => [row.campaign_id, accountId, row.campaign_name]);
};

/**
 * Maps data from the json array into an array
 * of values representing the rows of the line_item table
 * @param {Array} dataArray 
 */
const getLineItemRows = (dataArray) => {
  return dataArray
    // rows in the line_item table are
    // id PK, campaign_id FK, name, booked_amount, actual_amount
    .map(row => [row.id, row.campaign_id, row.line_item_name, row.booked_amount, row.actual_amount]);
};

/**
 * Maps data from the json array into an array
 * of values representing the rows of the adjustments table
 * @param {Array} dataArray 
 */
const getAdjustmentsRows = (dataArray) => {
  return dataArray
    // rows in the adjustments table are
    // id PK (omitted), line_item_id FK, amount, note (omitted)
    .map(row => [row.id, row.adjustments]);
};

/**
 * convenience function for creating insert placeholders
 * @param {int} columnCount 
 * @param {int} rowCount 
 */
const createPlaceholders = (columnCount, rowCount) => {
  const colStr = '('
    + new Array(columnCount)
      .fill('?')
      .join(',')
    + ')';
  return new Array(rowCount)
    .fill(colStr)
    .join(',\n')
    + ';\n';
};

/**
 * Takes the parameters and returns a function that takes
 * a transaction object as an arg and will
 * return a promise chain that will sequentially execute
 * insert statements where each insert statement inserts
 * maxRows number of rows.
 * 
 * Put another way, this creates a function that will
 * insert the rows you provide in a somewhat efficient
 * manner.
 * @param {array of arrays} rows row values
 * @param {string} tableName table to insert into
 * @param {array} columnNames array of column names
 * @param {int} maxRows MUST BE LESS THAN SQL_MAX_VALUES/columnNames.length
 */
const createInsertFunction = (rows, tableName, columnNames, maxRows) => {
  const baseScript = `
  INSERT INTO ${tableName}
  (${columnNames.join(',')})
  VALUES
  `;
  const rowChunks = chunk(rows, maxRows);
  const maxScript = baseScript + createPlaceholders(columnNames.length, maxRows);
  const tailScript = baseScript + createPlaceholders(columnNames.length, rows.length % maxRows || maxRows);

  // an array of objects
  // object.script is the sql to be passed to transaction.run
  // object.rowChunk is the param array to be passed ot transaction.run
  const insertObjs = rowChunks
    .map((rowChunk, i) => {
      let script = maxScript;
      if (i === rowChunks.length - 1) {
        script = tailScript;
      }
      return {
        script,
        values: flatten(rowChunk)
      }
    });

  return (transaction) => {
    return new Promise((resolve, reject) => {
      console.log(`-- inserting into "${tableName}"`)
      return Promise.all(insertObjs.map(({ script, values }) => {
        return transaction.run(script, values)
      }))
      .then(() => console.log(`-- done inserting ${tableName}`))
      .then(resolve)
      .catch((e) => reject(new Error(`unable to insert into "${tableName}"\nmessage:${e.message}`)));
    });
  };
};

/**
 * Takes the array representation of the sample json file an
 * creates an insert script for it.
 * @param {integer} accountId 
 * @param {Array} dataArray array of objects from the json file
 * @returns {string} the sql script that will insert the data
 */
const dataArrayToInsertFunctions = (accountId, maxRows, dataArray) => {
  let campaigns = getCampaignRows(accountId, dataArray);
  let lineItems = getLineItemRows(dataArray);
  let adjustments = getAdjustmentsRows(dataArray);
  return {
    campaigns: createInsertFunction(campaigns, 'campaign', ['id', 'account_id', 'name'], maxRows),
    lineItems: createInsertFunction(lineItems, 'line_item', ['id', 'campaign_id', 'name', 'booked', 'actual'], maxRows),
    adjustments: createInsertFunction(adjustments, 'adjustment', ['line_item_id', 'amount'], maxRows)
  };
};

const deleteDB = (dbFilePath) => {
  return Promise.all([
    fs.access(dbFilePath)
      .then(() => fs.unlink(dbFilePath))
      .catch(() => console.log(`not deleting ${dbFilePath}`)),
    fs.access(`${dbFilePath}-shm`)
      .then(() => fs.unlink(`${dbFilePath}-shm`))
      .catch(() => console.log(`not deleting ${dbFilePath}-shm`)),
    fs.access(`${dbFilePath}-wal`)
      .then(() => fs.unlink(`${dbFilePath}-wal`))
      .catch(() => console.log(`not deleting ${dbFilePath}-wal`))
  ])

  // return new Promise((resolve, reject) => {
    
  //   fs.access(dbFilePath)
  //     .then(() => {
  //       return Promise.all([
  //         fs.unlink(dbFilePath),
  //         fs.unlink(`${dbFilePath}-shm`),
  //         fs.unlink(`${dbFilePath}-wal`)
  //       ])
  //         .then(resolve)
  //         .catch(e => reject(new Error(`unable to delte existing db file ${dbFilePath}\nmessage:${e.message}`)))
  //     })
  //     .catch(() => resolve())
  // });
};

/**
 * Create the sqlite database and run migration
 * to create schema and populate necessary data
 * @param {string} dbFilePath 
 */
const createDB = (dbFilePath, migrationsPath) => {
  return new Promise((resolve, reject) => {
    let db;
    try {
      db = new Sqlite(dbFilePath);
    } catch (e) {
      reject(new Error(`unable to open connection to db\nmessage:${e.message}`));
    }
    db.migrate({ migrationsPath: migrationsPath })
      .then(() => resolve(db))
      .catch(e => reject(new Error(`unable to run database migration\nmessage:${e.message}`)));
  });
}

/**
 * sequentially runs the provided array of insert functions
 * created from createInsertFunction
 * @param {object} db sqlite db object
 * @param {array} insertFunctions 
 */
const runInsertFunctions = (db, insertFunctions) => {
  return new Promise((resolve, reject) => {
    db.transaction((trx) => {
      insertFunctions
        .reduce((lastProm, insertFunc) => {
          return lastProm
            .then(() => insertFunc(trx))
        }, Promise.resolve())
        .then(resolve)
        .catch(e => reject(e));
    });
  });
};

/**
 * A convenience function for logging in a promise chain
 * returns a function that logs the msg param, and returns
 * the first arg passed into it.
 * @param {string} msg 
 */
const promiseLog = (msg) => {
  return (prevResult) => {
    console.log(msg);
    return prevResult;
  };
};

/**
 * Creates and populates a sqlite db from the data json file.
 * Basically just creates a "then" chain of all the functios above
 * @param {object} configObj 
 */
const createDBFromDataFile = ({ dataFilePath, accountId, dbFilePath, migrationsPath, deleteExisting, maxRows }) => {
  console.log('CREATING DATABASE');
  let insertFunctions;
  return Promise.resolve()
    .then(promiseLog(`- reading data file ${dataFilePath}`))
    .then(() => readDataFile(dataFilePath))
    .then(promiseLog(`- creating insert script from data file`))
    .then(dataArrayToInsertFunctions.bind(null, accountId, maxRows))
    .then(resolvedInsertFuncs => insertFunctions = resolvedInsertFuncs)
    .then(() => {
      if (deleteExisting) {
        console.log(`- deleting existing database file @${dbFilePath}`);
        return deleteDB(dbFilePath);
      }
    })
    .then(promiseLog(`- creating database file @${dbFilePath}`))
    .then(createDB.bind(null, dbFilePath, migrationsPath))
    .then(promiseLog(`- running bulk insert script`))
    .then(db => runInsertFunctions(db, [insertFunctions.campaigns, insertFunctions.lineItems, insertFunctions.adjustments]))
    .then(promiseLog('DONE'))
    .catch((e) => {
      console.error('FATAL ERROR!');
      console.error(e.message);
    });
};

const getConfig = (configPath=null) => {
  let config = require('./config');
  if (configPath) {
    try {
      config = defaults(require(path.resolve(configPath)), config);
    } catch (e) {
      console.error(`unable to read config from "${configPath}"`);
      process.exit(-1);
    }
  }
  return config;
};

/*
* do the thing
*/
if (require.main === module) {
  /*
   * if the user provided a single argument, assume it's a path to a config file
   */
  let config;
  if (process.argv.length === 3) {
    config = getConfig(process.argv[2]);
  } else {
    config = getConfig();
  }

  createDBFromDataFile(config);
}

module.exports = {
  getConfig,
  createDBFromDataFile
}