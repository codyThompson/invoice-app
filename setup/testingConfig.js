const path = require('path');

module.exports = {
  dbFilePath: path.resolve(__dirname, '../invoice-app-test.sqlite'),

  // path to the json data file
  dataFilePath: path.resolve(__dirname, './smaller_test_data.json'),
};
