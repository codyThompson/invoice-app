path = require('path');

module.exports = {
  dbFilePath: path.resolve(__dirname, "../../invoice-app-test.sqlite")
};