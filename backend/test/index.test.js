const request = require('request-promise-native');
const path = require('path');
expect.extend(require('jest-json-schema').matchers);

process.argv[2] = path.resolve(__dirname, './testConfig');
const app = require('../src/index');
const port = process.env.port || 3001;
const testAccountId = 0;
let server = null;


const makeGetRequest = ({
  port=process.env.port || 3001,
  protocol='http',
  route='',
  param='',
  jsonResult=true,
  query={}
}) => {
  return request({
    uri: `${protocol}://localhost:${port}${route}${param}`,
    qs: query,
    json: jsonResult
  })
};

beforeAll(() => {
  return app.listen(port)
    .then(() => console.log('listening on ', port));
});

afterAll(() => {
  console.log('killing server');
  return app.close();
});

describe ('app', () => {
  describe('GET /v0/accounts/0', () => {
    test('it should return valid schema', () => {
      const resultSuccessSchema = {
        type: 'object',
        minProperties: 3,
        additionalProperties: false,
        properties: {
          success: {
            type: 'boolean'
          },
          errors: {
            type: 'array'
          },
          result: { // account
            type: 'object',
            minProperties: 5,
            additionalProperties: false,
            properties: {
              id: {
                type: 'integer'
              },
              name: {
                type: 'string'
              },
              totals: {
                type: 'object',
                minProperties: 4,
                additionalProperties: false,
                properties: {
                  booked: {
                    type: 'number'
                  },
                  actual: {
                    type: 'number'
                  },
                  adjustments: {
                    type: 'number'
                  },
                  billable: {
                    type: 'number'
                  }
                }
              },
              filtered_totals: {
                type: 'object',
                minProperties: 4,
                additionalProperties: false,
                properties: {
                  booked: {
                    type: 'number'
                  },
                  actual: {
                    type: 'number'
                  },
                  adjustments: {
                    type: 'number'
                  },
                  billable: {
                    type: 'number'
                  }
                }
              },
              campaigns: {
                type: 'array',
                minItems: 1, // we're expecting our test data to at least have one item
                items: {
                  type: 'object',
                  minProperties: 3,
                  additionalProperties: false,
                  properties: {
                    id: {
                      type: 'integer'
                    },
                    name: {
                      type: 'string'
                    },
                    totals: {
                      type: 'object',
                      minProperties: 4,
                      additionalProperties: false,
                      properties: {
                        booked: {
                          type: 'number'
                        },
                        actual: {
                          type: 'number'
                        },
                        adjustments: {
                          type: 'number'
                        },
                        billable: {
                          type: 'number'
                        }
                      }
                    }
                  }
                }
              }
              // end campaign level schema
            }
            // end account level schema
          }
        }
      };

      return makeGetRequest({
        route:'/v0/accounts/',
        param:testAccountId
      })
        .then((result) => {
          expect(result)
            .toMatchSchema(resultSuccessSchema);
        });
    });
  });
});