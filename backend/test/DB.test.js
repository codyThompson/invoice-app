const path = require('path');
expect.extend(require('jest-json-schema').matchers);

const DB = require('../src/DB');

let db;

beforeAll(() => {
  db = new DB(path.resolve(__dirname, '../../invoice-app-test.sqlite'));
});

afterAll(() => {
  return db.close();
})

describe('DB', () => {
  describe('processArgs', () => {
    test('it should return an error message if the field is not a valid sortField', () => {
      let result = db.processArgs('getAccount', 0, { sortField: 'foo' });
      expect(result.errorMessage)
        .toBeString();
      result = db.processArgs('getAccount', 0, { sortField: '' });
      expect(result.errorMessage)
        .toBeString();
    });
    test('it should return the expected filter statement', () => {
      let result = db.processArgs('getAccount', 0, {});
      expect(result.filterStmt)
        .toBe('');
      result = db.processArgs('getAccount', 0, {filter:'foo'});
      expect(result.filterStmt)
        .toBe(' and (campaign.name like ?) ');
      result = db.processArgs('getAccount', 0, {filter:'2'});
      expect(result.filterStmt)
        .toBe(' and (campaign.id = ? or campaign.name like ?) ');
      result = db.processArgs('getAccount', 0, {filter:2});
      expect(result.filterStmt)
        .toBe(' and (campaign.id = ? or campaign.name like ?) ');
      result = db.processArgs('getAccount', 0, {filter:'  foo baz bar '});
      expect(result.filterStmt)
        .toBe(' and (campaign.name like ?) ');
    });
    test('it should return the expected order by statement', () => {
      let result = db.processArgs('getAccount', 0, {});
      expect(result.orderByStmt)
        .toBe('');
      result = db.processArgs('getAccount', 0, {sortField: 'actual', sortAsc: false});
      expect(result.orderByStmt)
        .toBe(' order by campaign_totals.actual desc ');
      result = db.processArgs('getAccount', 0, {sortField: 'booked'});
      expect(result.orderByStmt)
        .toBe(' order by campaign_totals.booked asc ');
    });
    test('it should return the expected limit statement', () => {
      let result = db.processArgs('getAccount', 0, {});
      expect(result.limitStmt)
        .toBe('');
      result = db.processArgs('getAccount', 0, {limit: 22});
      expect(result.limitStmt)
        .toBe(' limit ? ');
      result = db.processArgs('getAccount', 0, {offset: 12});
      expect(result.limitStmt)
        .toBe(' offset ? ');
      result = db.processArgs('getAccount', 0, {limit: 7, offset: 303});
      expect(result.limitStmt)
        .toBe(' limit ?  offset ? ');
    });

    test('it should return the expected arg values', () => {
      let result = db.processArgs('getAccount', 0, {});
      expect(result.values)
        .toStrictEqual([0]);
      expect(result.singleRowValues)
        .toStrictEqual([0]);
      result = db.processArgs('getAccount', 0, {filter:'2'});
      expect(result.values)
        .toStrictEqual([0, 2, '%2%']);
      expect(result.singleRowValues)
        .toStrictEqual([0, 2, '%2%']);
      result = db.processArgs('getAccount', 0, {filter:'  mmk blah blah wow', sortField:'name'});
      expect(result.values)
        .toStrictEqual([0, '%mmk%blah%blah%wow%']);
      expect(result.singleRowValues)
        .toStrictEqual([0, '%mmk%blah%blah%wow%']);
      result = db.processArgs('getAccount', 0, {limit:20});
      expect(result.values)
        .toStrictEqual([0, 20]);
      expect(result.singleRowValues)
        .toStrictEqual([0]);
      result = db.processArgs('getAccount', 0, {offset:20, sortField: 'billable'});
      expect(result.values)
        .toStrictEqual([0, 20]);
      expect(result.singleRowValues)
        .toStrictEqual([0]);
    });
  });
  describe('getAccount', () => {
    test('it should resolve with the expected schema', () => {
      const resultSchema = {
        type: 'object',
        // TODO uncomment this when it's time for campaigns
        minProperties: 2,// make all properties required
        additionalProperties: false,
        properties: {
          account_info: {
            type: 'object',
            minProperties: 10,
            additionalProperties: false,
            properties: {
              id: {
                type: 'integer'
              },
              name: {
                type: 'string'
              },
              booked: {
                type: 'number'
              },
              actual: {
                type: 'number'
              },
              adjustments: {
                type: 'number'
              },
              billable: {
                type: 'number'
              },
              filtered_booked: {
                type: 'number'
              },
              filtered_actual: {
                type: 'number'
              },
              filtered_adjustments: {
                type: 'number'
              },
              filtered_billable: {
                type: 'number'
              }
            }
          }, // end account info
          campaigns: {
            type: 'array',
            minItems: 1, // make sure our test data actually includes 1 campaign
            items: {
              type: 'object',
              minProperties: 6,
              additionalProperties: false,
              properties: {
                id: {
                  type: 'integer'
                },
                name: {
                  type: 'string'
                },
                booked: {
                  type: 'number'
                },
                actual: {
                  type: 'number'
                },
                adjustments: {
                  type: 'number'
                },
                billable: {
                  type: 'number'
                },
              },
            }
          }
        }
      };

      return db.getAccount(0)
        .then(data => expect(data)
          .toMatchSchema(resultSchema));
    });

    test('it should result with the correct info and calculated values', () => {
      const camp1Booked = 430706.687153275 + 242951.095164073;
      const camp2Booked = 500000.0;
      const camp1Actual = 401966.505040068 + 244249.268603412;
      const camp2Actual = 400000.0;
      const camp1Adjustments = 1311.07311422303 + -2550.09011803598 + 200.0 + 300.0;
      const camp2Adjustments = -50000;
      const accountTotalBooked = camp1Booked + camp2Booked
      const accountTotalActual = camp1Actual + camp2Actual;
      const accountTotalAdjustments = camp1Adjustments + camp2Adjustments;
      return Promise.all([
        db.getAccount(0)
          .then(({ account_info }) => {
            expect(account_info.id)
              .toBe(0);
            expect(account_info.name)
              .toBe('fake account');
            expect(account_info.booked)
              .toBeCloseTo(accountTotalBooked, 5);
            expect(account_info.actual)
              .toBeCloseTo(accountTotalActual, 5);
            expect(account_info.adjustments)
              .toBeCloseTo(accountTotalAdjustments, 5);
            expect(account_info.billable)
              .toBeCloseTo(accountTotalActual + accountTotalAdjustments, 5);
            expect(account_info.filtered_booked)
              .toBeCloseTo(accountTotalBooked, 5);
            expect(account_info.filtered_actual)
              .toBeCloseTo(accountTotalActual, 5);
            expect(account_info.filtered_adjustments)
              .toBeCloseTo(accountTotalAdjustments, 5);
            expect(account_info.filtered_billable)
              .toBeCloseTo(accountTotalActual + accountTotalAdjustments, 5);
          }),
        db.getAccount(0, { filter: 'sat' })
          .then(({ account_info }) => {
            expect(account_info.id)
              .toBe(0);
            expect(account_info.name)
              .toBe('fake account');
            expect(account_info.booked)
              .toBeCloseTo(accountTotalBooked, 5);
            expect(account_info.actual)
              .toBeCloseTo(accountTotalActual, 5);
            expect(account_info.adjustments)
              .toBeCloseTo(accountTotalAdjustments, 5);
            expect(account_info.billable)
              .toBeCloseTo(accountTotalActual + accountTotalAdjustments, 5);
            expect(account_info.filtered_booked)
              .toBeCloseTo(camp1Booked, 5);
            expect(account_info.filtered_actual)
              .toBeCloseTo(camp1Actual, 5);
            expect(account_info.filtered_adjustments)
              .toBeCloseTo(camp1Adjustments, 5);
            expect(account_info.filtered_billable)
              .toBeCloseTo(camp1Actual + camp1Adjustments, 5);
          }),
        db.getAccount(0, { sortField: 'id' })
          .then(({ campaigns }) => {
            const [, camp2] = campaigns;
            expect(camp2.id)
              .toBe(2);
            expect(camp2.name)
              .toBe('fake campaign');
            expect(camp2.booked)
              .toBeCloseTo(camp2Booked, 5);
            expect(camp2.actual)
              .toBeCloseTo(camp2Actual, 5);
            expect(camp2.adjustments)
              .toBeCloseTo(camp2Adjustments, 5);
            expect(camp2.billable)
              .toBeCloseTo(camp2Actual + camp2Adjustments, 5);
          })
      ]);
    });
  });
});