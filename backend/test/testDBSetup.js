const path = require('path');
const Sqlite = require('sqlite-pool');

const {getConfig, createDBFromDataFile} = require('../../setup/setup');

const testSetupConfigPath = path.resolve(__dirname, '../../setup/testingConfig.js');

module.exports = async () => {
  console.log('\ncreating test db');
  const config = getConfig(testSetupConfigPath);
  await createDBFromDataFile(config);
  console.log('test db created. Adding some more test data');
  const sqlite = new Sqlite(config.dbFilePath);
  // TODO add multiple adjustment support to setup script
  await sqlite.exec(`
    insert into adjustment
    (line_item_id, amount, note)
    values
    (2, 200, 'test adjustment a'),
    (2, 300, 'test adjustment b')
  `);
  console.log('done adding extra test data');
};