const Sqlite = require('sqlite-pool');
const defaults = require('lodash/defaults');

class DB {
  constructor(dbFilPath, sqliteOptions={}) {
    this.sqlite = new Sqlite(dbFilPath, sqliteOptions);
  }

  /**
   * Provides statements and values arrays for
   * help with building queries. Validates that
   * the sort field is valid which is important
   * because it gets inserted directly into the query
   * @param {string} methodName the name of the DB class method being called
   * @param {integer} entityId added at the beginning of the values array
   * @param {object} args
   * @param {string or integer} args.filter the filter string
   * @param {string} args.sortField the child entity column to sort by
   * @param {integer} limit the max number of child rows to return
   * @param {integer} offset the number of child rows to offset by
   * @returns {object}
   *   - filterStmt: contains 'and (stuff like)' statement for where clause
   *   - orderByStmt: contains 'order by' statement 
   *   - limitStmt: contains 'limit ? offset ?' statement
   *   - values: contains the array of values to pass to the
   *       sqlite driver
   */
  processArgs (methodName, entityId, {
    filter=null,
    sortField=null,
    sortAsc=true,
    limit=null,
    offset=null
  }) {
    const fieldInfo = DB.fieldInfo[methodName];
    const fieldInfoVals = Object.values(fieldInfo);
    const result = {
      errorMessage: null,
      values: [entityId],
      singleRowValues: [entityId],
      filterStmt: '',
      orderByStmt: '',
      limitStmt: ''
    };

    if (sortField !== null && !(sortField in fieldInfo)) {
      result.errorMessage = `invalid sort field "${sortField}" validation is broken`;
      return result;
    }

    /*
    * create the filter statement and values
    */
    if (filter !== null) {
      let filterPieces = [];
      let filterNumOffset = 0;
      const filterNum = Number(filter);
      // add all the column = ? filter expressions
      if (!isNaN(filterNum)) {
        const filterNumPieces = fieldInfoVals
            .filter(info => info.canFilterBy && info.isInt)
            .map(info => `${info.queryColumn} = ?`)
        filterPieces = filterPieces.concat(filterNumPieces);
        filterNumOffset = filterNumPieces.length;
      }
      filter = ('' + filter)
        .trim()
        .split(' ')
        .join('%');
      filter = `%${filter}%`;
      // add all the column like ? filter expressions
      filterPieces = filterPieces.concat(
        fieldInfoVals
          .filter(info => info.canFilterBy && !info.isInt)
          .map(info => `${info.queryColumn} like ?`)
      );
      // create the statement
      const filterExpr = filterPieces
        .join(' or ');
      result.filterStmt = ` and (${filterExpr}) `;
      // add the values
      const filterVals = new Array(filterPieces.length)
        .fill(filterNum, 0, filterNumOffset)
        .fill(filter, filterNumOffset);
      result.values = result.values.concat(filterVals);
      result.singleRowValues = result.singleRowValues.concat(filterVals);
    }

    /*
    * create the order by statement
    */
    if (sortField !== null) {
      const sortDir = (sortAsc && 'asc') || 'desc';
      result.orderByStmt = ` order by ${fieldInfo[sortField].queryColumn} ${sortDir} `;
    }

    /*
    * create the limit and offset statement and values
    */
    if (limit !== null) {
      if (isNaN(Number(limit)) || limit < 1) {
        result.errorMessage = `invalid limit ${limit}, validation is broken`;
        return result;
      }
      result.limitStmt = ' limit ? ';
      result.values.push(limit);
    }
    if (offset !== null) {
      if (isNaN(Number(offset)) || offset < 1) {
        result.errorMessage = `invalid offset ${offset}, validation is broken`;
        return result;
      }
      result.limitStmt += ' offset ? ';
      result.values.push(offset);
    }

    return result;
  }

  /**
   * Query the db for account totals and child campaigns
   * @param {integer} account_id 
   * @param {object} args 
   * @param {string or integer} args.filter the filter string
   * @param {string} args.sortField the child entity column to sort by
   * @param {integer} args.limit the max number of child rows to return
   * @param {integer} args.offset the number of child rows to offset by
   */
  getAccount(accountId, args) {
    args = this.processArgs('getAccount', accountId, defaults(args, {sortField: 'id'}));

    if (args.errorMessage) {
      return Promise.reject(new Error(args.errorMessage));
    }

    const accountQuery = `
      select
        account.id as id,
        account.name as name,
        account_totals.booked as booked,
        account_totals.actual as actual,
        account_totals.adjustments as adjustments,
        account_totals.billable as billable,
        sum( campaign_totals.booked ) as filtered_booked,
        sum( campaign_totals.actual ) as filtered_actual,
        sum( campaign_totals.adjustments) as filtered_adjustments,
        sum( campaign_totals.billable ) as filtered_billable
      from campaign
  	  join campaign_totals on campaign_totals.campaign_id = campaign.id
      join account on account.id = campaign.account_id
      join account_totals on account_totals.account_id = account.id
      where account.id = ?
      ${args.filterStmt}
      group by account.id, account.name
    `;  
    const campaignsQuery = `
      select
      campaign.id as id,
      campaign.name as name,
      campaign_totals.booked,
      campaign_totals.actual,
      campaign_totals.adjustments,
      campaign_totals.billable
      from campaign
      join campaign_totals on campaign_totals.campaign_id = campaign.id
      where campaign.account_id = ?
      ${args.filterStmt}
      ${args.orderByStmt}
      ${args.limitStmt}
    `;

    return new Promise((resolve, reject) => {
      this.sqlite.transaction((trx) => {
        Promise.all([
          trx.get(accountQuery, args.singleRowValues),
          trx.all(campaignsQuery, args.values),
        ])
          .then(([account_info, campaigns]) => {
            return {account_info, campaigns}
          })
          .then(resolve)
          .catch(reject);
      });
    });
  }

  close () {
    return this.sqlite.close();
  }
}

DB.fieldInfo = {
  getAccount: {
    id: {
      queryColumn: 'campaign.id',
      canFilterBy: true,
      isInt: true
    },
    name: {
      queryColumn: 'campaign.name',
      canFilterBy: true
    },
    booked: {
      queryColumn: 'campaign_totals.booked'
    },
    actual: {
      queryColumn: 'campaign_totals.actual'
    },
    adjustments: {
      queryColumn: 'campaign_totals.adjustments'
    },
    billable: {
      queryColumn: 'campaign_totals.billable'
    }
  }
};

module.exports = DB;