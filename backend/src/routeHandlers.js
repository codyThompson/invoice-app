const {validationResult} = require('express-validator/check');
const defaults = require('lodash/defaults');

const buildPromiseChain = (promiseReturners, db, req, res) => {
  return promiseReturners
    .reduce((prevProm, currFunc) => {
      return prevProm.then(currFunc)
    }, Promise.resolve({
      db,
      req,
      res,
      result: null,
      errors: [],
      status: 200
    }));
};

const sendResponse = (res, resInfo) => {
  if (typeof resInfo !== 'object') {
    resInfo = {};
  }
  resInfo = defaults(resInfo, {
    errors: ['internal server error'],
    status: 500,
    result: null
  });
  return res
    .status(resInfo.status)
    .json({
      success: Array.isArray(resInfo.errors) && resInfo.errors.length === 0,
      errors: resInfo.errors,
      result: resInfo.result
    });
};

const getAccountFilter = (resInfo) => {
  return new Promise((resolve, reject) => {
    const accountId = resInfo.req.params.account_id;
    const {filter, limit, offset, ...query} = resInfo.req.query;
    debugger;
    let sortAsc;
    if (query.sort_dir) {
      sortAsc = query.sort_dir !== 'desc'
    }
    resInfo.db.getAccount(accountId, {
      filter,
      sortField: query.sort_field,
      sortAsc,
      limit,
      offset
    })
      .then(({account_info, campaigns}) => {
        if (!account_info) {
          resInfo.errors.push(`no account information available for the given account id and/or query`);
          resolve(resInfo);
          return;
        }
        try {
          const result = {
            id: account_info.id,
            name: account_info.name,
            totals: {
              booked: account_info.booked,
              actual: account_info.actual,
              adjustments: account_info.adjustments,
              billable: account_info.billable
            },
            filtered_totals: {
              booked: account_info.filtered_booked,
              actual: account_info.filtered_actual,
              adjustments: account_info.filtered_adjustments,
              billable: account_info.filtered_billable
            },
            campaigns: campaigns
              .map((row) => {
                return {
                  id: row.id,
                  name: row.name,
                  totals: {
                    booked: row.booked,
                    actual: row.actual,
                    adjustments: row.adjustments,
                    billable: row.billable
                  }
                }
              })
          };
          resInfo.result = result;
          resolve(resInfo);
        } catch (e) {
          reject(e);
        }
      });
  });
};

const handler = (filters, db, req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res
      .status(422)
      .json({
        success: false,
        errors: errors.array(),
        result: null
      })
  }

  buildPromiseChain(filters, db, req, res)
    .then(sendResponse.bind(null, res))
    .catch((e) => {
      console.error(`encountered unhandled error!!!\nmessage: ${e.message}\nstack: ${e.stack}`)
      res.status(500)
        .json({
          success: false,
          errors: ['internal server error'],
          status: 500,
          result: null
        });
    })
};

module.exports = {
  getAccount: handler.bind(null, [getAccountFilter]),
  empty: handler.bind(null, [])
};