const {param, query} = require('express-validator/check');

const isGreaterThan = (gt, val) => val > gt;
const isPositive = isGreaterThan.bind(-1);
const isGreaterThan0 = isGreaterThan.bind(0);

const getAccount = [
  param('account_id')
    .exists()
    .isInt()
    .custom(isPositive)
    .withMessage('must be a positive integer'),
  query('sort_field')
    .optional()
    .isIn(['id','name','booked','actual','adjustments','billable']),
  query('sort_dir')
    .optional()
    .isIn(['asc', 'desc']),
  query('filter')
    .optional()
    .isString(),
  query('offset')
    .optional()
    .isInt()
    .custom(isPositive)
    .withMessage('must be a positive integer'),
  query('limit')
    .optional()
    .isInt()
    .custom(isGreaterThan0)
    .withMessage('must be a greater than 0 integer')
];

module.exports = {
  get: {
    account: getAccount
  }
};