const express = require('express');
const cors = require('cors');

const validators = require('./validators');
const handlers = require('./routeHandlers');

const configPath = process.argv[2] || '../config';
const config = require(configPath);
const db = new (require('./DB'))(config.dbFilePath);

const app = express();
app.use(cors());

app.get('/', (req, res) => {
  res.json({hello: 'world'});
});

app.get('/v0/accounts/:account_id', validators.get.account, handlers.getAccount.bind(handlers, db));


// ugh - shoulda made a class
const appWrapper = {
  app,
  server: null,
  db: db,
  listen: (port) => {
    return new Promise((resolve, reject) => {
      try {
        appWrapper.server = app.listen(port, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve(appWrapper.server);
          }
        });
      } catch (e) {
        reject(e);
      }
    })
  },
  close: () => {
    db.close()
      .then(new Promise((resolve, reject) => {
        try {
          appWrapper.server.close((err) => {
            if (err) {
              return reject(err);
            }
            resolve();
          })
        } catch (e) {
          reject(e);
        }
      }));
  }
};

if (require.main === module) {
  const port = process.env.PORT || 3000;
  appWrapper.listen(port)
    .then(() => console.log('now listeneing on port', port))
    .catch(() => {
      console.log('error starting up');
      appWrapper.close()
        .catch();
    });
}


module.exports = appWrapper;